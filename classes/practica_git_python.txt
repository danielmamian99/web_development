Practica en GitLab (Generacion de data dummy)

Tomando como ejemplo la informacion que hay en www.amazon.com

Crear un repositorio donde cada integrante creara un un archivo de texto separados por algun caracter definido por ustedes para cada una de las siguientes
opciones:

1. Listado inicial de departments
    Ejemplo:
    id|name|active|create_date|city
    1|Digital Music|1|2019-06-10|United States
    2|Books|0|2019-06-10|United States
    ...

2. Listado de categorias, subcategorias y subsubcategoris por department
    2.1 Digital Music
        Ejemplo:
        id|Department|Category|SubCategory|active|createDate|city
        1|Digital Music|Stream Music|Amazon Music Unlimited|1|2019-06-10|United States
        2|Digital Music|Stream Music|Prime Music|1|2019-06-10|United States
        3|Digital Music|Buy Music|Best Sellers|1|2019-06-10|United States
        4|Digital Music|Buy Music|New Releases|1|2019-06-10|United States
        ...
    2.2 Books
    2.3 Computers
    2.4 Electronics
    2.5 Women's Fashion
    2.6 Men's Fashion
    2.7 Software
    2.8 Toys & Games

3. Crear un archivo de lenguages (ISO 639-2)

4. Un listado de usuarios (email, login, password, phone, city, name, middle name, last name, address, stars, city)

5. Generar un programa que permita crear N articulos con datos aleatorios relacionandolo a una subsubcategoria que se recibira
   por parametro:
    id|SubSubCategory|active|createDate|LongDescription|ShortDescription|By|Starts|QtyNew|ValueNew|QtyOld|ValueOld|ProductDetails...|language||United States
    1|Accounting & Finance|1|2019-06-10|Quicken 99 helps you make the most of your money! Find financial opportunities...|Quicken Basic 99 Windows|user|4.5|3|34.99|5|15.98|...||United States


    https://www.amazon.com/Intuit-251804-Quicken-Basic-Windows/dp/B00002S7W0/ref=sr_1_2?fst=as%3Aoff&qid=1560213897&refinements=p_n_operating_system_browse-bin%3A359861011&rnid=16225008011&s=software-intl-ship&sr=1-2

6. Generar un programa que permita crear M ofertas con datos aleatorios pero teniendo en cuenta que esos articulos deben pertenecer al sistema
   id|idArticle|was|Deal of the day|createDate|endDate|YouSave|By|City

   Precio anterior: 	US$175.98
   Oferta del Día: 	US$127.99
   Quedan 05h 37m 59s
   Ahorras: 	US$47.99 (27%)

   Tenga en cuenta que M <= N

Nota: Al final debe quedar una sola version del repositorio sin conflictos ni pull-requests pendientes
